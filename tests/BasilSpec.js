var basilIterationTime = 160;

describe("BasilJS", function() {
  var app;

  afterEach(function() {
	if (typeof app === "object") { 
    	app.__destruct();
    }
  });

  describe("model", function () {
	  it("should be retrievable via getter", function() {
		app = new BasilJS();
		app.model.test = 'Hello';
		expect(app.getModel('test')).toEqual('Hello');
	  }); 
	  
	  it("should recognise a direct change to model object", function(done) {
		var val = Math.random();
	    var callback = function (newModel) {
		    expect(newModel).toEqual(val);
		    done();
		};
	
		app = new BasilJS();
		app.addListener('model.test', callback);
		app.model.test = val;
	  });
	  
	  it("should recognise a change using setter", function(done) {
		var val = Math.random();
	    var callback = function (newModel) {
		    expect(newModel).toEqual(val);
		    done();
		};
	
		app = new BasilJS();
		app.addListener('model.test', callback);
		app.setModel('test', val);
	  });
	  
	  it("should recognise a change to children", function(done) {
	    var callback = function (newModel) {
		    expect(newModel.child).toEqual('Hello');
		    done();
		};
	
		app = new BasilJS();
		app.model.test = {};
		app.addListener('model.test', callback);
		app.model.test.child = 'Hello';
	  }); 

	  it("should recognise a direct deletion of model object", function(done) {
	    var callback = function (newModel) {
		    expect(newModel).toBeUndefined();
		    done();
		};
	
		app = new BasilJS({disableDebug: false});
		app.model.debug = true;
		app.model.test = 'Hello';
		
		setTimeout(function () {
			app.addListener('model.test', callback);
			delete app.model.test;
		}, basilIterationTime);
	  }); 

	  it("should recognise a deletion by deleter", function(done) {
	    var callback = function (newModel) {
		    console.log(newModel);
		    expect(newModel).toBeUndefined();
		    done();
		};
	
		app = new BasilJS();
		app.model.test = 'Hello';
		setTimeout(function () {
			app.addListener('model.test', callback);
			app.deleteModel('test');
		}, basilIterationTime);
	  }); 
	  
	  it("should be retrievable from a cache", function (done) {
		 app = new BasilJS({cacheName: 'jasmine'});
		 app.model.test = 'TEST';
		 
		 setTimeout(function () {
			 app.__destruct();
			 
			 app = new BasilJS({cacheName: 'jasmine'});
			 expect(app.model.test).toEqual('TEST');
			 done();
		 }, 250);
	  });

  });
  
  describe("listeners", function () {
	 it("should work on an existing model", function (done) {
		var callback = function (newModel) {
			expect(newModel).toEqual('two');
			done();
		}
		app = new BasilJS();
		app.model.test = 'one';
		app.addListener('model.test', callback);
		app.model.test = 'two';
	 });
	 
	 it("should work on an undefined model", function (done) {
		var val = Math.random();
		var callback = function (newModel) {
			expect(newModel).toEqual(val);
			done();
		}
		app = new BasilJS();
		app.addListener('model.test', callback);
		app.model.test = val;
	 });
	 it("should be countable", function () {
		app = new BasilJS();
		app.addListener('model.test', function () {});
		expect(app.listenerCount('model.test')).toEqual(1);
	 });
	 it("when broken, should be automatically trashed", function (done) {
		app = new BasilJS({removeBrokenListeners: true});
		app.addListener('model.test', function () { unknownobject; });
		app.model.test = 'One';
		setTimeout(function () {
			expect(app.listenerCount('model.test')).toEqual(0);
			done();
		}, basilIterationTime);
	 });
	 it("should always point to the correct listener function", function (done) {
		var callback = function (newModel) {
			console.log('offlinemodel.three changed', newModel);
			expect(newModel).toEqual('three');
			done();
		}
		app = new BasilJS();
		var one = app.addListener('offlinemodel.one', function (newModel) { console.log('offlinemodel.one changed', newModel); });
		var two = app.addListener('offlinemodel.two', function (newModel) { console.log('offlinemodel.two changed', newModel); });
		var three = app.addListener('offlinemodel.three', callback);
		var four = app.addListener('offlinemodel.four', function (newModel) { console.log('offlinemodel.four changed', newModel); });
		var five = app.addListener('offlinemodel.five', function (newModel) { console.log('offlinemodel.five changed', newModel); });
		
		app.removeListener(one);
		app.removeListener(four);
		
		var six = app.addListener('offlinemodel.six', function (newModel) { console.log('offlinemodel.six changed', newModel); });
		var seven = app.addListener('offlinemodel.six', function (newModel) { console.log('offlinemodel.seven changed', newModel); });
		
		app.removeListener(five);
		app.removeListener(six);
		
		var eight = app.addListener('offlinemodel.eight', function (newModel) { console.log('offlinemodel.eight changed', newModel); });
		var nine = app.addListener('offlinemodel.nine', function (newModel) { console.log('offlinemodel.nine changed', newModel); });
		
		app.offlinemodel = {};
		app.offlinemodel.one = 'one';
		app.offlinemodel.two = 'two';
		app.offlinemodel.three = 'three';
		app.offlinemodel.four = 'four';
		app.offlinemodel.five = 'five';
		app.offlinemodel.six = 'six';
		app.offlinemodel.seven = 'seven';
		app.offlinemodel.eight = 'eight';
		app.offlinemodel.nine = 'nine';

	 });
	 it("should call a child listener when a parent model is added/changed", function (done) {
		var val = Math.random();
		app = new BasilJS();
	
		app.addListener('model.parent.child', function (newModel) {
			expect(newModel).toEqual(val);
			done();
		});
		
		app.model.parent = {child: val};
	 });
	 it("should call a child listener when a parent model is deleted", function (done) {
		app = new BasilJS();
		app.addListener("model", function () {
			console.log("Changes detected...", this);
		});
		app.model.parent = {child: Math.random()};
		
		var callCount = 0;
		app.addListener('model.parent.child', function (newModel) {
			//console.log("callback run", callCount, this);
			callCount++;
			if (callCount == 1)
				return;
				
			expect(newModel).toBeUndefined();
			done();
		});

		setTimeout(function () {
			delete app.model.parent;
		}, basilIterationTime);
		
	 });
	 /*it("should call a child listener when a parent listener is called (manually via runListeners())", function (done) {
		app = new BasilJS();
	
		app.addListener('parent.child', function (newModel) {
			expect(newModel).toEqual('child value');
			done();
		});
		
		app.runListeners('parent', {child: 'child value'});
	 });*/
  });
  
  
});
