<?php
	
	error_reporting(E_ALL);
	ini_set("display_errors", false);
	
	include("../../../json.inc");
	
	$calledApi = split("/", $_SERVER['PHP_SELF']);
	$calledApi = $calledApi[count($calledApi)-1];
	
	if (isset($_GET['data']))
		$data = $_GET['data'];
	else
		$data = urldecode(file_get_contents('php://input'));
	if ($data != "")
	    $requestObj = json_decode($data, true);
	
	//$requestObj["ranme"] = array("fail" => true);
	

	
	$returnObj = array();
	
	foreach($requestObj as $action => $parameters) {
		if (isset($parameters["fail"]) && ($parameters["fail"] === true || $parameters["fail"] == "true")) {
			$returnObj[$action] = array(
				"success" => false,
				"data" => array (),
				"errors" => array("Error simulated")
			);
		} else {
			$returnObj[$action] = array(
				"success" => true,
				"data" => array (
					"random" => rand(),
					"api" => $calledApi,
					"action" => $action
				),
				"errors" => array()
			);
		}
	}
	
	header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
	header("Access-Control-Allow-Credentials: true");
	header("Content-Type: application/json");
	header("P3P:CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");
	
	if (isset($_GET['jsonp']))
		echo $_GET['jsonp'] . "(";
	
	echo json_encode($returnObj);
	
	
	if (isset($_GET['jsonp']))
		echo ");";