BasilJS
======

[![Build Status](https://img.shields.io/badge/build-passing-yellow.svg)](https://bitbucket.org/raininglemons/basiljs)

Basic Usage
--------
Instantiating app object

```
#!javascript
var app = new BasilJS();

```

Retrieving variables


```
#!javascript

// Using function (preferred)
var variableValue = app.getVariable("model.one");
// Directly
var variableValue = app.model.one;
```


Changing variables

```
#!javascript
// Using function (preferred)
app.setVariable("model.five", true);
// Directly
app.model.five = true;
```

Adding listeners

```
#!javascript
app.addListener("model.five", callback_function);
```

TODO
--------
* Add support for expiration of data
* Watch url
* * pushState
* * legacy support (for browsers w/o pushState)
* Watch url hash
* ~~React to custom variables being sent from other instances~~
* ~~Build skeleton to process {{variables}}~~
* ~~Build skeleton to run external helpers~~
* Build if helper
* Build show helper
* Build css helper
* Build skeleton api functionality
* Build default api adaptor
* Test test test

Documentation
--------

* [API Docs](docs)