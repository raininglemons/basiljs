YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "BasilJS",
        "BasilJS.Utils",
        "BasilJS.Utils.Template",
        "window.localStorage"
    ],
    "modules": [
        "BasilJS",
        "localStorage polyfill"
    ],
    "allModules": [
        {
            "displayName": "BasilJS",
            "name": "BasilJS",
            "description": "Creates the app object with a model which is synced over localStorage\nto any other windows using the same data store.\nMonitors the model and runs any user defined listeners to the changes.\n\nConstruction: var app = new BasilJS ( optionConfig );\n\nAnything under the variable app.model.* automatically persists and is\nshared across instances of your app. Future variables can persist by\ndefining them in cachedVariables {Array} with updateConfig or on app\nconstruction."
        },
        {
            "displayName": "localStorage polyfill",
            "name": "localStorage polyfill",
            "description": "Polyfills the localStorage callback in IE 8\nPolyfills localStorage in IE 6-7"
        }
    ]
} };
});