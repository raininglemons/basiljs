/**
 *
 *	#scope helper
 *
 *	Will make some data available in the current element and its children
 *
 */	
BasilJS.prototype.helpers["scope"] = function (statement, e) {
	/*var valid = this.Utils.Template.getVariable(statement);
	
	console.log(this.instance, valid, statement, e);
	
	this.addListener(statement);
	
	if (valid === undefined || !valid || valid == "")
		e.style.display = 'none';
	else
		e.style.display = '';
		
	e.setAttribute("data-modifiedcount", this.instance);*/
	//console.log("Running data scope", statement, e);
	try {
		var d = JSON.parse(statement);
		this.scope = this.Utils.mergeObjects(d, this.scope);
	} catch (e) {
		console.warn(e, "data-scope: unable to read JSON string");
	}
};

// Sets the helper type to a "modifier"
// 
// This means you are handed an element, and you can manipulate it
// directly. DO NOT REMOVE IT. IT WILL NOT COME BACK!!
//
BasilJS.prototype.helpers["scope"].prototype.access = "modify";