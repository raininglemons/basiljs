/**
 *
 *	#if helper
 *
 *	Will add or remove an element, if the statement passed to it is valid
 *
 */	
BasilJS.prototype.helpers["if"] = function (statement, e) {
	var valid = this.Utils.Template.getVariable(statement);
	
	console.log(this.instance, valid, statement, e);
	
	this.addListener(statement);
	
	if (valid === undefined || !valid || valid == "")
		return null
	else {
		e.setAttribute("data-instance", this.instance);
		return e;
	}
};

// Sets the helper type to a "manipulator"
// 
// This means you are handed an element and must either return it
// or if it isn't to be rendered, instead return null.
//
BasilJS.prototype.helpers["if"].prototype.access = "manipulate";