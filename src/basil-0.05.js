/**
 *	Creates the app object with a model which is synced over localStorage
 *	to any other windows using the same data store.
 *	Monitors the model and runs any user defined listeners to the changes.
 *
 *	Construction: var app = new BasilJS ( optionConfig );
 *
 *	Anything under the variable app.model.* automatically persists and is
 *	shared across instances of your app. Future variables can persist by
 *	defining them in cachedVariables {Array} with updateConfig or on app
 *	construction.
 *
 *	@module BasilJS
 *	@class BasilJS
 *	@constructor
 */
var BasilJS = function (startupConfig) {
	"use strict";
	
	var _this = this,
		BasilJS = function () {},
		app = new BasilJS(),
		__compatability = {
			objectObserve: Object.observe ? true : false,
			localStorage: window.localStorage ? true : false,
			isArray: Array.isArray !== undefined
		};
	
	//=====================================================================
	//
	//                          .model
	//
	//=====================================================================
	//	
	// Retrieve model from storage first.
	//
	var __shadowModel = {};
	var __modelDirtyObserver = [];
	var __activeConfig = {};
	var __defaultConfig = {
		autoStart: true, 
		cachedVariables: ["model"],
		localStoragePrefix: "basilJS", 
		removeBrokenListeners: false
	};
	var __listenerBlock = [];

	//	
	//						    Object.observe	
	//
	// First and foremost let's try and use .observe. It's sooo much easier
	// and cleaner to use. But not nearly as widely supported. Meh
	//
	/**
	 *	First and foremost let's try and use .observe. It's sooo much easier
	 *	and cleaner to use. But not nearly as widely supported. Meh
	 *	@method createObserver
	 *	@private
	 *	@param obj {Object} Object to observe
	 *	@param chain {String} Name of object relative to the app object. IE app.model -> model, app.url.hash -> url.hash, app.myArray[10] -> myArray[10]
	 *	@param shouldRunListeners {Boolean} Whether or not the listeners of the passed object's children should be immeadiatly called.
	 */
	function createObserver (obj, chain, shouldRunListeners) {
		var modelPath = chain || [];
		shouldRunListeners = shouldRunListeners || false;
		//console.log("creating observer on", obj, modelPath, shouldRunListeners);
		
		Object.observe(obj, function(changes) {
			//console.log(changes);
			for (var i = 0; i < changes.length; i++) {
				
				var thisChain = modelPath.slice(0);
				thisChain.push(changes[i].name);
				//console.log(1, changes[i]);
				//console.log(2, thisChain, changes[i].object[changes[i].name], changes[i].oldValue, true, true);
				//try {
					//console.log("should run listener for", thisChain);
					runListeners(thisChain, changes[i].object[changes[i].name], changes[i].oldValue, true, true);
				//} catch (e) {
				//	console.log("Error", e);
				//	console.log(e.stack);
				//}
				//console.log(3, "listener passed?")
				
				if (changes[i].object !== null && changes[i].object !== undefined && typeof changes[i].object[changes[i].name] === "object" && typeof changes[i].oldValue !== "object") {
					createObserver(changes[i].object[changes[i].name], thisChain, true);
				}
				//console.log("herro");

			}
			
			updateShadow();
		});

		//console.log("hi");
		
		// Check any child objects
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				var thisChain = modelPath.slice();
				thisChain.push(key);
				if (obj[key] !== null && obj[key] !== undefined && typeof obj[key] === "object") {
					createObserver (obj[key], thisChain, shouldRunListeners);
				}
				if (shouldRunListeners) {
					//console.log("should run listener for", thisChain);
					runListeners(thisChain, obj[key], undefined, false, true);
				}
			}
		}
	}
	
	//
	//						Dirty checking
	//
	//
	/*
	 *	Dirty checking alternative to Object.observe when not avaliable
	 *
	 *	@method dirtyObserver
	 *	@private
	 */
	function dirtyObserver (/*model, shadow, chain*/ ) {
		/*console.log(chain);
		chain = chain || ["model"];
		console.log(chain);
		model = model || app.model;
		console.log(model, getShadow(chain, true), chain);*/
		//return false;
		//if (dirtyIterator(model, getShadow(chain, true), chain)) {
		/*if (*/ dirtyIterator (app, __shadowModel, []); //) {
			//console.log("should run listener for")
			//runListeners(chain, model, getShadow(chain, true), false);
			//try {
			//if (__activeConfig.cachedVariables.indexOf(chain[0]) >= 0)
			//	saveToStorage();
			//else
				updateShadow(); //chain, model);
			//} catch (e) {
			//	console.log("error saving to storage!");
			//	console.log(e.message);
			//	console.log(e.stack);
			//} 
		//}
		//console.log("hows my shadow?", __shadowModel);
		
		function dirtyIterator (obj, shadow, chain) {
			//console.log(obj, shadow, chain);
			var changes = false;
      		var newChain;
			
			for (var key in obj) {
				if (obj.hasOwnProperty(key)) {
					newChain = chain.slice(0);
					newChain.push(key);
					if (typeof obj[key] === "object" && shadow[key] !== undefined && typeof shadow[key] === "object") {
						if (dirtyIterator(obj[key], shadow[key], newChain)) {
							//console.log("should run listener for", newChain);
							runListeners (newChain, obj[key], shadow[key], false);
							changes = true;
						}
					} else if (obj[key] !== shadow[key] && typeof obj[key] !== "function") {
						//console.log("should run listener for", newChain, typeof obj[key]);
						runListeners(newChain, obj[key], shadow[key], true);
						changes = true;
					}
					delete shadow[key];
				}
			}
			
			for (var key in shadow) {
				if (shadow.hasOwnProperty(key)) {
					newChain = chain.slice(0);
					newChain.push(key);
					console.log("should run listener for", newChain);
					runListeners (newChain, obj[key], shadow[key], true, false);
					//console.log("changes", newChain);
					changes = true;
				}
			}
			
			return changes;
		}
	}

	
	//=====================================================================
	//
	//                       localStorage interface
	//
	//=====================================================================
	//	
	//						localStorageCallback
	//
	//	Reacts to changes to the model
	//	TODO: make this recursive...
	//
	/**
	 *	Callback function to update model on changes of localStorage by other
	 *	instances of app
	 *
	 *	@method localStorageCallback
	 *	@private
	 *	@param event {StorageEvent} StorageEvent provided by localStorage
	 */
	 // TODO!!!
	function localStorageCallback (e) {
		console.log("storage changed"); //, e);
		switch (e.key) {
			case __activeConfig.localStoragePrefix + "-model":
			var alteredModel = JSON.parse(e.newValue);
			for (var key in app.model) {
				if (app.model.hasOwnProperty(key)) {
					if (alteredModel[key] === undefined)
						delete app.model[key];
					else
						app.model[key] = alteredModel[key];
					delete alteredModel[key];
				}
			}
			for (var key in alteredModel) {
				if (alteredModel.hasOwnProperty(key))
					app.model[key] = alteredModel[key];
			}
			break;
		}
		//console.log("model updated");
	}

	//
	//							loadFromStorage
	//
	//	Manually pulls from storage
	//	TODO: check storage supported
	//
	/**
	 *	Manually pulls data from storage (used to initially populate model)
	 *
	 *	@method loadFromStorage
	 *	@private
	 *	@param stem {String} Suffix of key to pull from localStorage
	 *	@param shouldParse {Boolean} Whether to try and process a JSON response, (false always returns the raw string)
	 *	@output cache {Mixed} Entry in localStorage for given key
	 */
	function loadFromStorage (stem, parse) {
		parse = parse || true;
		var cache = window.localStorage.getItem(__activeConfig.localStoragePrefix + "-" + stem);
		if (cache === null)
			return {};
		try {
			return parse ? JSON.parse(cache) : cache;
		} catch (e) {
			// Probably not a json object for some reason
			return cache;
		}
	}

	//
	//							saveToStorage
	//
	//	Saves the active model to localStorage
	//	TODO: pay attention to user defined variables to store
	//
	/**
	 *	Saves the active model to localStorage
	 *
	 *	@method saveToStorage
	 *	@private
	 *	@param stem {String} Suffic of key to save model to localStorage with
	 */
	function saveToStorage (stem) {
		console.log("Saving to storage"); //, __listenerBlock);

		stem = stem ? stem : "model";
		var json = JSON.stringify(app[stem]);
		if (loadFromStorage(stem, false) != json)
			window.localStorage.setItem(__activeConfig.localStoragePrefix + "-" + stem, json);

		//updateShadow();
		// Now update the shadow
		//__shadowModel[type] = getModel("", true);
		//console.log("new __shadowModel", __shadowModel);

		// Reset the block
		//__listenerBlock = [];
		//console.log("hi mum", getModel("", true), __shadowModel, app.model);
	}
	
	//=====================================================================
	//
	//                          getModel
	//
	//=====================================================================
	//	
	// Function to retrieve a model value
	//
	/**
	 *	Returns (if avaliable) the value of a given key within the model.*
	 *
	 *	@method getModel
	 *	@param chain {String}	Variable in model to retrieve
	 *	@param shouldCopy {Boolean} Whether to return a copy of the requested variable
	 *	@deprecated Use `getVariable` with the model. prefix
	 */
	function getModel(chain, copy) {
		copy = copy || false;
		chain = cleanChain(chain);
		/*if (chain[0] == "model")
			chain.splice(0, 1);
		var searchHandle = app.model;
		try {
			for (var i = 0; i < chain.length; i++)
				searchHandle = searchHandle[chain[i]];
		} catch (e) {
			return undefined;
		}
		if (copy)
			return JSON.parse(JSON.stringify(searchHandle));
		else
			return searchHandle;*/
		return getVariable(["model"].concat(chain), copy);
	}
	
	/**
	 *	Sets the value of a value in model.*
	 *
	 *	@method setModel
	 *	@param chain {String} Variable in model to change
	 *	@param value {Mixed} Value to set to variable
	 *	@deprecated Use `setVariable` with the model. prefix
	 */
	function setModel(chain, value) {
		chain = cleanChain(chain);
		setVariable (["model"].concat(chain), value);
		/*console.log(chain);
		if (chain[0] == "model")
			chain.splice(0, 1);
		var searchHandle = app.model;
		for (var i = 0; i < chain.length - 1; i++) {
			if (typeof searchHandle[chain[i]] !== "object")
				if (parseInt(chain[i+1]) == chain[i+1])
					searchHandle[chain[i]] = [];
				else
					searchHandle[chain[i]] = {};
			searchHandle = searchHandle[chain[i]];
			console.log(searchHandle);
		}
		
		searchHandle[chain[chain.length - 1]] = value;
		console.log(app.model.test, searchHandle);*/
	}
	
	/**
	 *	Deletes a variable from the model.*
	 *
	 *	@method deleteModel
	 *	@param chain {String} Variable in model to delete
	 *	@deprecated Use `deleteVariable` with the model.* prefix
	 */
	function deleteModel (chain) {
		chain = cleanChain(chain);
		deleteVariable (["model"].concat(chain));
	}
	
	/**
	 *	Returns any variable within the app object where avaliable
	 *
	 *	@method getVariable
	 *	@param chain {String} Variable in app object to retrieve
	 *	@param shouldCopy {Boolean} Whether to return a copy of the requested variable
	 */
	function getVariable (chain, copy) {
		copy = copy || false;
		//var e = new Error();
		//console.log("getVariable", chain);
		//console.log(e.stack);
		chain = cleanChain(chain);
		//if (chain[0] == "model")
		//	chain.splice(0, 1);
		var searchHandle = app || {};
		try {
			for (var i = 0; i < chain.length; i++) {
				//console.log(" - ", searchHandle);
				searchHandle = searchHandle[chain[i]];
			}
			//console.log(" - ", searchHandle);
		} catch (e) {
			return undefined;
		}
		if (copy)
			return Utils.copyObject(searchHandle);
		else
			return searchHandle;		
	}
	
	/**
	 *	Sets any variable within the app object
	 *
	 *	@method setVariable
	 *	@param chain {String} Variable in app object to set
	 *	@param value {Mixed} Value to set for given variable
	 */
	function setVariable (chain, value) {
		chain = cleanChain(chain);
		//console.log(chain);
		var searchHandle = app;
		for (var i = 0; i < chain.length - 1; i++) {
			if (typeof searchHandle[chain[i]] !== "object")
				if (parseInt(chain[i+1]) == chain[i+1])
					searchHandle[chain[i]] = [];
				else
					searchHandle[chain[i]] = {};
			searchHandle = searchHandle[chain[i]];
			//console.log(searchHandle);
		}
		
		searchHandle[chain[chain.length - 1]] = value;
		//console.log(app.model.test, searchHandle);		
	}
	
	/**
	 *	Deletes a variable from the app object
	 *
	 *	@method deleteVariable
	 *	@param chain {String} Variable in app to delete
	 */
	function deleteVariable (chain) {
		chain = cleanChain(chain);
		var searchHandle = app;
		for (var i = 0; i < chain.length - 1; i++) {
			if (typeof searchHandle[chain[i]] !== "object")
				return true;
			searchHandle = searchHandle[chain[i]];
			//console.log(searchHandle);
		}
		
		delete searchHandle[chain[chain.length - 1]];		
	}

	/**
	 *	Retrieves shadow copy of a provided variable
	 *
	 *	@method getShadow
	 *	@private
	 *	@param chain {String} Variable in app object shadow to retrieve
	 *	@param shouldCopy {Boolean} Whether to return a copy of the requested variable
	 *	@return shadow {Mixed}
	 */
	function getShadow(chain, copy) {
		copy = copy || false;
		//var e = new Error();
		//console.log("getShadow", chain);
		//console.log(e.stack);
		chain = cleanChain(chain);
		//if (chain[0] == "model")
		//	chain.splice(0, 1);
		var searchHandle = __shadowModel;
		try {
			for (var i = 0; i < chain.length; i++) {
				//console.log(" - ", searchHandle);
				searchHandle = searchHandle[chain[i]];
			}
			//console.log(" - ", searchHandle);
		} catch (e) {
			return undefined;
		}
		if (copy)
			return Utils.copyObject(searchHandle);
		else
			return searchHandle;
	}
	
	/**
	 *	Updates the shadow with the current app variables
	 *
	 *	@method updateShadow
	 *	@private
	 */
	function updateShadow(chain, model) {
		//chain = cleanChain(chain);
		//__shadowModel[chain[0]] = model;
		__shadowModel = Utils.copyObject(app);
		
		// Release the block on listeners
		__listenerBlock = [];
	}
	/**
	 *	Converts a variable into a "chain" variable. Used internally
	 *	to ensure all variables are formatted in a uniform fashion.
	 *
	 *	IE app.model.tree -> ["model", "tree"]
	 *	   app.url.bat[10] -> ["url", "bat", 10]
	 *
	 *	@method cleanChain
	 *	@private
	 *	@param chain {Mixed} Variable to convert to "chain" array
	 *	@return chain {Array}	"chain" array
	 */
	function cleanChain (chain) {
		if (typeof chain == "string") {
			chain = chain.replace("]", "").replace("[", ".");
			chain = chain.split(".");
			if (chain.length == 1 && chain[0] === "")
				chain = [];
		}
		// Duplicate chain, so we can manipulate it without
		// causing problems up the stack
		//console.log("cleanChain", chain);
		return chain.length ? chain.slice(0) : [];
	}

	//=====================================================================
	//
	//                         Start / Stop
	//
	//=====================================================================
	//	
	// Function to retrieve a model value
	//
	/**
	 *	Initialises the app's data from localStorage and starts observing
	 *	the app's variables.
	 *
	 *	@method start
	 */
	function start () {
		// Using Object.observe
		//var storageCopy = {};
		for (var i = 0; i < __activeConfig.cachedVariables.length; i++)
			app[__activeConfig.cachedVariables[i]] = loadFromStorage(__activeConfig.cachedVariables[i]);
			
		//app.model = storageCopy !== null ? storageCopy : {};
		app.url = {href: window.location.href, hash: window.location.hash};
		__shadowModel = Utils.copyObject(app);

		if (__compatability.objectObserve) {
			//try {
				console.log("Using Object.observe to monitor model");
				createObserver (app, []);
				//createObserver (app.url, ["url"]);
			//} catch (e) {
				// Observer not supported
			//	console.log(e);
			//	__compatability.objectObserve = false;
			//}
		}
		// Resort to dirty checking
		if (!__compatability.objectObserve) {
			// Run dirty checker
			//__shadowModel.model = copyObject(app.model);
			//__shadowModel.url = copyObject(app.url);
			
			//console.log("__shadowModel.url", __shadowModel.url);
			console.log("Using dirty checking to monitor model");
			//__modelDirtyObserver.push(setInterval(function () { dirtyObserver(app.model, __shadowModel["model"], ["model"]); }, 100));
			__modelDirtyObserver.push(setInterval(function () { /*console.log("interval running");*/ dirtyObserver(app.url, __shadowModel["url"], ["url"]); }, 100));
		}

		// Assume we can use localStorage. Else we're kind of boned.
		try {
			if (window.addEventListener) {
			  	window.addEventListener("storage", localStorageCallback, false);
			} else {
				window.attachEvent("onstorage", localStorageCallback, false);
			}
		} catch (e) {
			// No localStorage enabled.
			console.log("Warning: No localStorage support. Caching disabled.");
			__compatability.localStorage = false;
		}
	}
	
	/**
	 * 	Stops the dirty iterator. Does NOT stop observing variables with
	 *	Object.observe.
	 *
	 *	@method stop
	 */
	function stop () {
		for (var i = 0; i < __modelDirtyObserver.length; i++)
			clearInterval(__modelDirtyObserver[i]);
	}
	
	/**
	 *	Updates the config of the app object. Automatically run on construction when
	 *	a config is passed.
	 *
	 *	@method updateConfig
	 *	@param newConfig {Object} any new parameters
	 */
	 function updateConfig (newConfig) {
		 Utils.mergeObjects(newConfig, __activeConfig, false);
	 }
	
	//=====================================================================
	//
	//                         Listeners
	//
	//=====================================================================
	//	
	// 
	//
	var __listeners = [];
	var __listenersIndex = {};
	var __listenersEmpty = [];
	
	/**
	 *	Runs listeners (if avaliable) for a given variable
	 *
	 *	@method runListeners
	 *	@param watchVar {String} Variable to run listeners for
	 *	@param varValue {Mixed} New value
	 *	@param oldValue {Mixed} Old value
	 *	@param runChildren {Boolean} Should run children listeners
	 *	@param runParents {Boolean} Should run parent listeners
	 */
	function runListeners (watchVar, varValue, oldValue, runChildren, runParents) {
		//console.log("runListeners", watchVar);
		
		watchVar = cleanChain(watchVar);
		/*runChildren = runChildren || false;
		runParents = runParents || false;
		var topOfTree = app; //topOfTree || app[watchVar[0]] || {unknown: true};
		
		//console.log("trying to run", watchVar);
		// Let' actually check if theres been a change?
		//console.log(watchVar, JSON.stringify(varValue) !== JSON.stringify(oldValue), JSON.stringify(varValue), JSON.stringify(oldValue), runChildren ? "runChildren" : "", runParents ? "runParents" : ""); */
		if (JSON.stringify(varValue) === JSON.stringify(oldValue))
			return false;
			
		
		//console.log("__listenersIndex", __listenersIndex);
		//var tmpIndexPosition = __listenersIndex;
		/*for (var i = 0; i < watchVar.length && tmpIndexPosition !== undefined; i++) {
			tmpIndexPosition = tmpIndexPosition[watchVar[i]];
			console.log(watchVar.slice().splice(0, i+1).join("."), tmpIndexPosition);
			
			if ((runParents || i == watchVar.length-1) && !__listenerBlock[watchVar.slice().splice(0, i+1).join(".")]) {
				// We can run this one.
				//console.log("running ", watchVar.slice().splice(0, i+1));
				
				if (tmpIndexPosition !== undefined && typeof tmpIndexPosition["__listeners"] === "object") {
					var listenersToRun = tmpIndexPosition["__listeners"].slice(0);
					
					for (var i = 0; i < listenersToRun.length; i++)
						try {
							var tmpThis = {
								listenerId: listenersToRun[i],
								listenerVariable: __listeners[listenersToRun[i]][0],
								remove: function () { removeListener.call(app, listenersToRun[i]); },
								varValue: varValue,
								oldValue: oldValue,
								model: topOfTree
							};
							__listeners[listenersToRun[i]][1].call(tmpThis, varValue, oldValue);
						} catch (e) {
							// Error
							console.log("Listener failed", e);
							console.log(e.stack);
							
							// Autoremove broken listeners?
							if (__activeConfig["removeBrokenListeners"])
								removeListener (listenersToRun[i]);
						}
				}
				
				__listenerBlock[watchVar.slice().splice(0, i+1).join(".")] = true;
			}
		}*/
		
		// Run parents first
		if (runParents && watchVar.length > 1) {
			var parent = watchVar.slice().splice(0, watchVar.length - 1);
			//console.log(" - parents", parent, getVariable(parent), getShadow(parent));
			runListeners (parent, getVariable(parent), getShadow(parent), false, true);
			/*var parentVal = app;
			for (var i = 0; i < parent.length; i++) {
				console.log(parentVal);
				parentVal = parentVal[parent[i]];
			}
			console.log("Parent?", parent, parentVal, getShadow(parent), false, true, topOfTree);
			runListeners(parent, parentVal, getShadow(parent), false, true, topOfTree);*/
		}
		
		//console.log("runListeners", watchVar);
		if (__listenerBlock[watchVar.join(".")] === undefined && !__listenerBlock[watchVar.slice().splice(0, i+1).join(".")]) {
			//console.log("running listener", watchVar, varValue, oldValue);
			
			// Now run any listeners...
			var listenersToRun = __listenersIndex;
			for (var i = 0; i < watchVar.length && listenersToRun !== undefined; i++)
				listenersToRun = listenersToRun[watchVar[i]];
			//console.log(" - Listeners to run", listenersToRun !== undefined ? listenersToRun["__listeners"] : null, watchVar);
			//console.log(__listeners);
			if (listenersToRun !== undefined && listenersToRun["__listeners"] !== undefined) {
				for (var i = 0; i < listenersToRun["__listeners"].length; i++) {
					try {
						var tmpThis = {
							listenerId: listenersToRun["__listeners"][i],
							listenerVariable: __listeners[listenersToRun["__listeners"][i]][0],
							remove: function () { removeListener.call(app, listenersToRun["__listeners"][i]); },
							varValue: varValue,
							oldValue: oldValue,
							model: app
						};
						__listeners[listenersToRun["__listeners"][i]][1].call(tmpThis, varValue, oldValue);
						//__listeners[listenersToRun["__listeners"][i]]();
					} catch (e) {
						console.log(" - Couldn't run listener id " + listenersToRun["__listeners"][i]);
						console.log(e.message);
						console.log(e.stack);
						if (__activeConfig.removeBrokenListeners) {
							removeListener(listenersToRun["__listeners"][i]);
						}
					}
				}
			}
			
			//console.log(__activeConfig.cachedVariables, watchVar[0], typeof __activeConfig.cachedVariables.indexOf, __activeConfig.cachedVariables.indexOf);
			if (watchVar.length == 1 && __activeConfig.cachedVariables.indexOf(watchVar[0]) >= 0) {
				// Should save the variable to the model
				//console.log("Save " + watchVar[0], __activeConfig.cachedVariables.indexOf(watchVar[0]));
				saveToStorage(watchVar[0]);
			}
			
			// Block it from being called twice for one change (may happen when using
			// Object.observe
			__listenerBlock[watchVar.join(".")] = true;
		} else {
			//console.log("not running listener", watchVar);
		}
		
		//return true;
		
		// Run req var 2nd, parent may make any listeners here redundant
		//console.log("Changed!", watchVar);
		/*var listenersToRun = __listenersIndex;
		for (var i = 0; i < watchVar.length && listenersToRun !== undefined; i++)
			listenersToRun = listenersToRun[watchVar[i]];
		//console.log(" - Listeners to run", listenersToRun !== undefined ? listenersToRun["__listeners"] : null, watchVar);
		//console.log(__listeners);
		if (listenersToRun !== undefined && listenersToRun["__listeners"] !== undefined) {
			for (var i = 0; i < listenersToRun["__listeners"].length; i++) {
				try {
					var tmpThis = {
						listenerId: listenersToRun["__listeners"][i],
						listenerVariable: __listeners[listenersToRun["__listeners"][i]][0],
						remove: function () { removeListener.call(app, listenersToRun["__listeners"][i]); },
						varValue: varValue,
						oldValue: oldValue,
						model: topOfTree
					};
					__listeners[listenersToRun["__listeners"][i]][1].call(tmpThis, varValue, oldValue);
					//__listeners[listenersToRun["__listeners"][i]]();
				} catch (e) {
					console.log(" - Couldn't run listener id " + listenersToRun["__listeners"][i]);
					console.log(e.message);
					console.log(e.stack);
				}
			}
		}*/
		
		// Run children last, they may not be needed if theres some listener manipulating
		// the dom further up the model
		if (runChildren && (typeof varValue === "object" || typeof oldValue === "object")) {
			//console.log("run children of", watchVar);
			// Let's find any child listeners
			var tmpIndexPosition = __listenersIndex;
			for (var i = 0; i < watchVar.length && tmpIndexPosition !== undefined; i++) {
				tmpIndexPosition = tmpIndexPosition[watchVar[i]];
			}
			
			if (tmpIndexPosition !== undefined)
				for (var key in tmpIndexPosition) {
					if (tmpIndexPosition.hasOwnProperty(key) && key !== "__listeners") {
						// Recursively run this function for each child
						var childVarValue = typeof varValue === "object" ? varValue[key] : undefined;
						var childOldValue = typeof oldValue === "object" ? oldValue[key] : undefined;
						var childVar = watchVar.slice();
						childVar.push(key);
						//console.log(" - children", childVar);
						runListeners(childVar, childVarValue, childOldValue, true, false);
					}
				}
		
			//console.log(tmpIndexPosition);
		}
	}
	/**
	 *	Deletes a listener
	 *
	 *	@method	removeListener
	 *	@param listenerID {Integer} ID of listener to remove
	 */
	function removeListener (listenerIndex) {
		var listenerToRemove;
		if (listenerIndex == __listeners.length - 1) {
			// Listener is the last one in the array, safe to splice
			listenerToRemove = __listeners.splice(listenerIndex, 1)[0];
		} else {
			// Can't safely splice it without distrupting the array
			// indexes. Overrite it to undefined and add entry in
			// __listenersEmpty to mark it as free
			listenerToRemove = __listeners[listenerIndex];
			__listeners[listenerIndex] = undefined;
			__listenersEmpty.push(listenerIndex);
		}
		
		// Now remove its entry from __listenersIndex
		//console.log(listenerToRemove);
		var cleanedChain = cleanChain(listenerToRemove[0]);
		//console.log("Removing", cleanedChain)
		var tmpIndexPosition = __listenersIndex;
		for (var i = 0; i < cleanedChain.length; i++) {
			console.log(tmpIndexPosition);
			console.log(cleanedChain[i]);
			tmpIndexPosition = tmpIndexPosition[cleanedChain[i]];
		}
		
		for (var i = 0; i < tmpIndexPosition["__listeners"].length; i++) {
			if (tmpIndexPosition["__listeners"][i] == listenerIndex) {
				tmpIndexPosition["__listeners"].splice(i, 1);
				break;
			}
		}
		
		return true;
	}
	
	/**
	 *	Adds a listener for a variable
	 *
	 *	@method addListener
	 *	@param bindVariable {String} Variable to bind listener function to
	 *	@param bindFunction {Function} Callback function to run on change of variable value
	 *	@return listenerID {Integer} Unique ID of listener, to be used with removeListener
	 */
	function addListener (bindVariable, bindFunction) {
		// Check if there are any empty listener spots
		var listenerIndex = __listeners.length;
		if (__activeConfig.removeBrokenListeners && __listenersEmpty.length > 0) {
			var listenerIndex = __listenersEmpty.splice(0, 1)[0];
		}
		//console.log("Pushing to pos: ", listenerIndex, typeof listenerIndex, bindVariable);
		__listeners[listenerIndex] = [bindVariable, bindFunction];
		
		// Let's break the bind variable down into its "tree" / chain
		var cleanedChain = cleanChain(bindVariable);
		var tmpIndexPosition = __listenersIndex;
		for (var i = 0; i < cleanedChain.length; i++) {
			if (tmpIndexPosition[cleanedChain[i]] === undefined)
				tmpIndexPosition[cleanedChain[i]] = {};
				
			tmpIndexPosition = tmpIndexPosition[cleanedChain[i]];
		}
		
		if (tmpIndexPosition["__listeners"] === undefined)
			tmpIndexPosition["__listeners"] = [];
			
		tmpIndexPosition["__listeners"].push(listenerIndex);
		
		//console.log("__listeners", __listeners);
		//console.log("__listenersIndex", __listenersIndex);
		//console.log("__listenersEmpty", __listenersEmpty);
		
		return listenerIndex;
	}
	
	/**
	 *	Returns the count of stored listeners for a variable
	 *
	 *	@method listenerCount
	 *	@param	bindVariable {String} Return count of listeners bound to this variable
	 *	@param	includeChildren {Boolean} Should include child listeners in count
	 *	@return	listenerCount {Integer} Number of listeners bound to variable (and children if requested)
	 */
	 function listenerCount (bindVariable, includeChildren) {
		bindVariable = cleanChain(bindVariable);
		console.log("listenerCount", bindVariable, bindVariable.slice().push("meep"));
		includeChildren = includeChildren !== undefined ? includeChildren : false;
		
		var listenersToRun = __listenersIndex;
		for (var i = 0; i < bindVariable.length && listenersToRun !== undefined; i++)
			listenersToRun = listenersToRun[bindVariable[i]];

		var foundListeners = 0;

		if (listenersToRun !== undefined && listenersToRun["__listeners"] !== undefined) {
			foundListeners += listenersToRun["__listeners"].length;
			
			if (includeChildren)
				for (var key in listenersToRun) {
					if (listenersToRun.hasOwnProperty(key) && key !== "__listeners") {
						var childVar = bindVariable.slice();
						childVar.push(key);
						foundListeners += listenerCount(childVar, true);
					}
				}
		}
		return foundListeners;
	 }
	
	/**
	 *	Provides a number of reusable functions, not necessarily tied
	 *	just to their usage in basil.
	 *
	 *	@class BasilJS.Utils
	 *	@since 0.1.1
	 *	@static
	 */
	var Utils = {};
	
	/**
	 * Duplicates an object and returns a new copy
	 *
	 *	@method copyObject
	 *	@param obj {Object} Object to duplicate
	 *	@return duplicateObject {Object} Duplicate object
	 */
	Utils.copyObject = function (obj) {
		if (typeof obj === "object")
			return JSON.parse(JSON.stringify(obj));
		else
			return obj;
	}
	
	/**
	 *	Combines to objects (with the first object passed given precedence
	 *
	 *	@method mergeObjects
	 *	@param	obj1 {Object} Object with higher precendence
	 *	@param	obj2 {Object} Object with lower precendence
	 *	@param	copy {Boolean} should duplicate objects before combining (Default: false)
	 *	@return	mergedObj {Object} Combined object
	 */
	Utils.mergeObjects = function (obj1, obj2, copy) {
		copy = copy !== undefined ? copy : true;
		if (copy)
			obj2 = Utils.copyObject(obj2);
		for (var key in obj1) {
			if (obj1.hasOwnProperty(key))
				if (typeof obj1[key] == "object") {
					obj2[key] = Utils.isArray(obj1[key]) ? [] : {};
					Utils.mergeObjects(obj1[key], obj2[key], false);
				} else {
					obj2[key] = obj1[key];
				}
		}
		return obj2;
	}
	
	/**
	 *	Returns true if the passed object is an array
	 *
	 *	@method isArray
	 *	@param	obj {Object} Object to check
	 *	@return	isArray {Boolean} returns true if obj is an array
	 */
	Utils.isArray = function (obj) {
		if (__compatability.isArray)
			return Array.isArray(obj);
		else
			return obj.constructor == Array;
	}

	/**
	 *	Crossbrowser event listening to input value change via either
	 *	the user, or the browser autofill.
	 *
	 *	@method onChange
	 *	@param	e {DOMElement} Element to listen to
	 *	@param	callback {Function} Function to run on change
	 */	
	Utils.onChange = function (e, callback) {
		if (e.addEventListener !== undefined) {
			e.addEventListener("keyup", function (event) {
				callback(this.value);
			});
			e.addEventListener("paste", function (event) {
				callback(this.value);
			});
		} else {
			e.attachEvent("onkeyup", function (event) {
				callback(this.value);
			});
			e.attachEvent("onpaste", function (event) {
				callback(this.value);
			});
		}
		var documentIsInteractive = false;
		var intervalHandle = setInterval(function () {
			if (documentIsInteractive) {
				// Need to wait on the next iteration as the autofill
				// values aren't always avaliable just after document
				// ready.
				clearInterval(intervalHandle);
				callback(e.value);
				//console.log("Checking autofill", e.value);				
			} else if (document.readyState == "complete") {
				documentIsInteractive = true;
			}
		}, 10);
	};
	
	/**
	 *	Provides a number of functions for binding and linking the DOM
	 *	to the model
	 *
	 *	Initialised with app.Utils.Template.compile(target); with target
	 *	pointing towards the top level DOM element to parse.
	 *
	 *	@class BasilJS.Utils.Template
	 *	@since 0.1.1
	 *	@static
	 */
	(function (Utils) {
		var __listenerIndex = {},
			__templates = [],
			t = {
				/**
				 *	Prefix to use when mapping template variables to model variables.
				 *
				 *	For example; with a prefix "model",
				 *		{{player}}  -->  app.model.player
				 *		{{site.name}} --> app.model.site.name
				 *	@property variableScope
				 *	@type	String
				 *	@default	""
				 */
				variableScope: ""
			};
			
		/**
		 *	Maps a template variable to it's equivalent in the model
		 *
		 *	@method mapVariableToModel
		 *	@private
		 *	@param	variable {String} Variable to map
		 *	@return	modelVariable {String} Paired variable in model
		 */	
		function mapVariableToModel (variable) {
			// TODO
			if (t.variableScope != "")
				return t.variableScope + "." + variable;
			else
				return variable;
		}

		/**
		 *	Function to process any handlebars syntax within a single text node
		 *
		 *	@method parseString
		 *	@private
		 *	@param	node {DOMElement} Node to output result
		 *	@param	template {String} String to parse
		 *	@param	scope {Object} Extra data to make avaliable to the template (in addition to the model)
		 *	@param	bindListeners {Boolean} Whether or not listeners should be bound to node
		 *	@return listeners {Array <ListenerID>} returns a list of created listeners
		 */		
		function parseString (node, template, scope, bindListeners) {
			//console.log(node, template, scope);
			bindListeners = bindListeners || false;
			var output = template,
				listeners = [];
			// Update a node with the parse string that should be there
			// Add blocking logic...
			
			var variables = output.match(/{{([^}])+}}/g);
			for (var i = 0; i < variables.length; i++) {
				var cleanVariable = variables[i].substr(2, variables[i].length-4),
					scopedVal = t.getScope(cleanVariable, scope),
					val = t.getVariable(cleanVariable); //mapVariableToModel(cleanVariable));
				
				//console.log(cleanVariable, scopedVal, val, scope);
				output = output.replace(variables[i], scopedVal || val || ""); //val === undefined ? "" : val);
				
				if (bindListeners && !scopedVal) {
					// Create listeners...
					//console.log("creating listener for", cleanVariable);
					listeners.push(addListener(mapVariableToModel(cleanVariable), function () {
						// Add blocking logic here...
						if (!node || node.parentNode === null)
							this.remove();
						else
							parseString(node, template, scope, false);
					}));
				}
			}
			//console.log(node, node.nodeValue, output);
			
			node.nodeValue = output;
			//console.log(node, node.nodeValue, output);
			
			return listeners;
		}
		
		/**
		 *	Function to process any <input>'s and apply a two way data bind
		 *	where specified with the data-bind='' tag.
		 *
		 *	TODO, check listener is being passed back...
		 *
		 *	@method bindInput
		 *	@private
		 *	@param	node {DOMElement} Node to apply two way data bind
		 *	@param	scope {Object} Extra data to make avaliable to the template (in addition to the model)
		 *	@return listeners {Array <ListenerID>} returns a list of created listeners
		 */	
		function bindInput (node) {
			var variable = node.getAttribute("data-bind"),
				listeners = [];
			if (variable === null)
				return;
			
			var val = getVariable(mapVariableToModel(variable));
			node.value = val === undefined ? "" : val;

			listeners.push(addListener(mapVariableToModel(variable), function (newValue) {
				if (!node || node.parentNode === null)
					this.remove();
				else
					node.value = newValue;
			}));	
			
			Utils.onChange(node, function (newVal) {setVariable(mapVariableToModel(variable), newVal); });
			
			return listeners;
		}
		
		/**
		 *	Function to process any handlebars syntax within a single text node
		 *
		 *	@method compile
		 *	@constructor
		 *	@param	e {DOMElement} Element on DOM to start compiling from
		 *	@param scope {Object} Extra data to make avaliable to the template (in addition to the model)
		 */	
		t.compile = function (e, scope, toplevel) {
			
			//console.log(e, scope);
			
			var _compileThis = this;
			
			this.toplevel = toplevel || [];
			this.scope = scope || {};
			this.e = e;
			this.clone = e.cloneNode(true);
			this.childNodes = [];
			this.listeners = [];
			this.removeListeners = function (childrenOnly) {
				childrenOnly = childrenOnly || false;
				if (!childrenOnly) {
					// remove listeners!
					for (var li = 0; li < _compileThis.listeners.length; li++) {
						removeListener(_compileThis.listeners[li]);	
					}	
					_compileThis.listeners = [];
				}
				
				// now pass the command down to child nodes
				for (var li = 0; li < _compileThis.childNodes.length; li++) {
					_compileThis.childNodes[li].removeListeners();	
				}	
			};
			
			
			var nodes = this.clone.childNodes;
			if (e.attributes)	
				for (var ai = 0; ai < e.attributes.length; ai++) {
					if (e.attributes[ai].name.substr(0, 5) === "data-" && _this.helpers[e.attributes[ai].name.substr(5)] !== undefined) {
						//console.log("invoking", e.attributes[ai].name);
						var helper = e.attributes[ai].name.substr(5),
							statement = e.attributes[ai].value,
							helper_listeners = {},
							helper_this = {
								Utils: Utils,
								instance: 0,
								scope: _compileThis.scope,
								getTemplate: function (chain) {
									return t.getTemplate(chain, scope, this.toplevel);	
								},
								addListener: function (chain) {
									if (helper_listeners[chain] === undefined) {
										//console.log("adding listener...", chain);
										helper_listeners[chain] = addListener(mapVariableToModel(chain), function () {
												//console.log("trying to run helper listener...");
												_compileThis.e = execute_helper(_compileThis.e, _compileThis.clone);
											});
										_compileThis.listeners.push(helper_listeners[chain]);
									}
								},
								removeListener: function (chain) {
									if (helper_listeners[chain] !== undefined) {
										//console.log("removing listener...", helper_listeners[chain]);
										removeListener (helper_listeners[chain]);
										_compileThis.listeners.splice(_compileThis.listeners.indexOf(helper_listeners[chain]), 1);
										delete helper_listeners[chain];
									}
								}
							},
							execute_helper = function (e, clone) {
								helper_this.instance++;
								//try {
									switch (_this.helpers[helper].prototype.access) {
										case "modify":
										_this.helpers[helper].call(helper_this, statement, _compileThis.e);
										break;
										
										case "manipulate": // Manipulate...
										var result = _this.helpers[helper].call(helper_this, statement, clone.cloneNode(true));
										//console.log(helper + " result:", result);
										
										if (result === null) {
											result = document.createComment("placeholder[" + helper_this.instance + "] - " + helper + ": " + statement);
											e.parentNode.insertBefore(result, e);
											e.parentNode.removeChild(e);
											e = result;
											
											// Remove child listeners
											_compileThis.removeListeners(true);
											
											// Remove child node references (they're now gone. ouch)
											//_compileThis.childNodes = [];
										} else {
											// Logic to check if nodes are the same?
											// or if we can optimise our manipulation of the dom
		
											e.parentNode.insertBefore(result, e);
											e.parentNode.removeChild(e);
											e = result;
											
											// Need to reparse child nodes
											processNodes(e);
										}
										//console.log(e);
										break;
									}
									_compileThis.scope = helper_this.scope;
									//console.log(_compileThis.scope);
									return e;
								//} catch (er) {
								//	console.log("Error running " + helper, er);	
								//}
							};
							
							this.e = execute_helper(this.e, this.clone);
					}
				}
				
			function processNodes (e) {
				var nodes = e.childNodes;
				for (var i = 0; i < nodes.length; i++) {
					if (nodes[i].nodeType == 3 || nodes[i].nodeType == 8) {
						if (nodes[i].nodeValue.match(/{{([^}])+}}/)) {
							var string_listeners = parseString(e.childNodes[i], nodes[i].nodeValue, _compileThis.scope, _compileThis.toplevel, true);
							for (var il = 0; il < string_listeners.length; il++)
								_compileThis.listeners.push(string_listeners[il]);
						}
					} else if (nodes[i].nodeType == 1 && nodes[i].localName == "input") {
						//console.log("Binding two way input");
						var input_listeners = bindInput(e.childNodes[i], _compileThis.scope, _compileThis.toplevel);
						for (var il = 0; il < input_listeners.length; il++)
							_compileThis.listeners.push(input_listeners[il]);
					} else if (nodes[i].nodeType == 1) {
						// Recursively look
						_compileThis.childNodes.push(new t.compile(e.childNodes[i], _compileThis.scope, _compileThis.toplevel));
					}
					//console.log(nodes[i], nodes[i].nodeType);
				}
			}
			
			processNodes(e, nodes);
		};
		
		/**
		 *	Function to process a statement composed of model variables and
		 *	return a true or false result (incomplete)
		 *
		 *	@method executeStatement
		 *	@param	statement {String}
		 *	@return result {Boolean}
		 */
		t.executeStatement = function (statement) {
			return false;	
		}

		/**
		 *	Helper function for template helpers to access variables within their
		 *	variable scope
		 *
		 *	@method getVariable
		 *	@param chain {String} Variable in app object to retrieve
		 *	@param shouldCopy {Boolean} Whether to return a copy of the requested variable
		 *	@param scope {Object} Extra data to make avaliable to the template (in addition to the model)
		 *	return value {Mixed}
		 */	
		t.getVariable = function (chain, copy) {
			chain = mapVariableToModel(chain);
			return getVariable(chain, copy);	
		};
		
		t.getScope = function (chain, scope) {
			if (scope) {
				//console.log("Searching scope for ", chain, scope);
				chain = cleanChain(chain);
				
				var searchHandle = scope || {};
				try {
					for (var i = 0; i < chain.length; i++) {
						//console.log(" - ", searchHandle);
						searchHandle = searchHandle[chain[i]];
					}
					//console.log(" - ", searchHandle);
				} catch (e) {
					return undefined;
				}
				return searchHandle;
			}
		}
		
		/**
		 *	Helper function for template helpers to set variables within their
		 *	variable scope
		 *
		 *	@method setVariable
		 *	@param chain {String} Variable in app object to set
		 *	@param value {Mixed} New value
		 */	
		t.setVariable = function (chain, value) {
			chain = mapVariableToModel(chain);
			return setVariable(chain, value);	
		};
		
		//console.log("hallo",u);
		Utils.Template = t;
		//return u;
	})(Utils);
	 
	updateConfig(__defaultConfig);
	
	if (startupConfig !== undefined)
		updateConfig(startupConfig);
		
	console.log("Running with config:", __activeConfig);

	if (__compatability.objectObserve) {
		Object.defineProperty(app, "getModel", {
			value: getModel,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "setModel", {
			value: setModel,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "deleteModel", {
			value: deleteModel,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "addListener", {
			value: addListener,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "runListeners", {
			value: runListeners,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "removeListener", {
			value: removeListener,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "listenerCount", {
			value: listenerCount,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "start", {
			value: start,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "stop", {
			value: stop,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "updateConfig", {
			value: updateConfig,
			writable: false,
			configurable: false,
			enumerable: false
		});
		// Legacy
		Object.defineProperty(app, "__destruct", {
			value: stop,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "getVariable", {
			value: getVariable,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "setVariable", {
			value: setVariable,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "deleteVariable", {
			value: deleteVariable,
			writable: false,
			configurable: false,
			enumerable: false
		});
		Object.defineProperty(app, "Utils", {
			value: Utils,
			writable: false,
			configurable: false,
			enumerable: false
		});
	} else {
		// Can't use define property
		var append = BasilJS.prototype !== undefined ? BasilJS.prototype : app;
		
		append.getModel = getModel;
		append.setModel = setModel;
		append.deleteModel = deleteModel;
		append.addListener = addListener;
		append.runListener = runListeners;
		append.removeListener = removeListener;
		append.listenerCount = listenerCount;
		append.start = start;
		append.stop = stop;
		append.updateConfig = updateConfig;
		append.__destruct = stop;
		append.getVariable = getVariable;
		append.setVariable = setVariable;
		append.deleteVariable = deleteVariable;
		append.Utils = Utils;
	}
	
	if (__activeConfig["autoStart"] !== undefined && __activeConfig["autoStart"])
		app.start();
	
	return app;
};

BasilJS.prototype.helpers = {};