/*
 *	Simple lightweight localstorage polyfill for IE 5-7
 *
 *	Does not support the direct changing of the localStorage object.
 *	Use localStorage.setItem() instead
 *
 *	You can however retrieve items directly from the localStorage object
 *	i.e.	localStorage.getItem("item1") == localStorage.item1
 *
 *	WARNING: direct access like localStorage.item1 is only updated on
 *  initialisation. So doesnt account for changes caused by other pages
 *  running simulanteously. localStorage.getItem() does, so this is
 *	prefered.
 */
/**
 *	Polyfills the localStorage callback in IE 8
 *	Polyfills localStorage in IE 6-7
 *
 *	@module localStorage polyfill
 *	@class	window.localStorage
 */
(function (window, document) {
	"use strict";
	
	var polyfill = function () {};
	
	/**
	 *	Sets an item
	 *
	 *	@method setItem
	 *	@param key {String} key of item to set
	 *	@param value {String} value of item to set
	 *	@return success {Boolean} returns true if successful
	 */
	polyfill.prototype.setItem = function (key, value) {
				this.__get();
				this[key] = value;
				shadowCopy[key] = value;
				var rawData = "";
				polyfill.prototype.length = 0;
				for (var key in this) {
					if (this.hasOwnProperty(key)) { // && key != "") {
						rawData += escape(key) + ":" + escape(this[key]) + "\n";
						polyfill.prototype.length ++;
					}
				}
				rawData = rawData.length ? rawData.substr(0, rawData.length - 1) : "";
				storageHandle.setAttribute("polyfilled", rawData);
				storageHandle.save("cache");

				polyfill.prototype.size = rawData.length;
				return true;
			};

	/**
	 *	Retrieves an item
	 *
	 *	@method getItem
	 *	@param key {String} key of item to retrieve
	 *	@return value {String} value of item
	 */
	polyfill.prototype.getItem = function (key) {
				// Refresh data
				this.__get();
				return this[key];
			};

	/**
	 *	Removes an item
	 *
	 *	@method removeItem
	 *	@param key {String} key of item to remove
	 *	@return success {Boolean} returns true if successful
	 */
	polyfill.prototype.removeItem = function (key) {
				try {
					this.__get();
					delete this[key];
					delete shadowCopy[key];
					var rawData = "";
					polyfill.prototype.length = 0;
					for (var key in this) {
						if (this.hasOwnProperty(key)) { // && key != "") {
							rawData += escape(key) + ":" + escape(this[key]) + "\n";
							polyfill.prototype.length ++;
						}
					}
					rawData = rawData.length ? rawData.substr(0, rawData.length - 1) : "";
					storageHandle.setAttribute("polyfilled", rawData);
					storageHandle.save("cache");
					
					polyfill.prototype.size = rawData.size;
					return true;
				} catch (e) {
					console.log(e.message, e);
				}
			};

	/**
	 *	Clears and item from memory
	 *
	 *	@method clear
	 *	@param key {String} key of item to clear
	 *	@return success {Boolean} returns true if successful
	 */
	polyfill.prototype.clear = function (key) {
				try {
					storageHandle.setAttribute("polyfilled", "");
					storageHandle.save("cache");
					polyfill.prototype.length = 0;
					polyfill.prototype.size = 0;
					for (var key in this) {
						if (this.hasOwnProperty(key))
							delete this[key];
					}
					shadowCopy = {};
					return true;
				} catch (e) {
					console.log(e.message, e);
				}
			};
			
	/**
	 *	Polyfill for the localStorage even listener
	 *
	 *	@method __eventListener
	 *	@private
	 */
    /*polyfill.prototype.*/ var __eventListener = function () {
    	if (typeof window.localStorage.__get !== "undefined") {
    		// Running polyfill
		    var shadowCopy2 = {};
		    for (var key in shadowCopy) {
			    if (shadowCopy.hasOwnProperty(key))
				    shadowCopy2[key] = shadowCopy[key];
		    }
	        for (var key in window.localStorage) {
	            if ((window.localStorage.hasOwnProperty === undefined || window.localStorage.hasOwnProperty(key)) && (typeof shadowCopy[key] !== "boolean" ? (shadowCopy[key] ? shadowCopy[key].toString() : "false") : shadowCopy[key]) + "" != window.localStorage[key]) {
	                // Local copy differs to shadow copy, save changes
	                window.localStorage.setItem(key, window.localStorage[key]);
	                shadowCopy[key] = window.localStorage[key];
	                delete shadowCopy2[key];
	            } else if (window.localStorage.hasOwnProperty === undefined || window.localStorage.hasOwnProperty(key)) {
		            delete shadowCopy2[key];
	            }
	        }
	        for (var key in shadowCopy2) {
		        //look for locally deleted vars
		        	window.localStorage.removeItem(key);
	        }
        	window.localStorage.__get();
        }
        var newShadowCopy = {};
        for (var key in window.localStorage) {
            if ((window.localStorage.hasOwnProperty === undefined || window.localStorage.hasOwnProperty(key)) && (typeof shadowCopy[key] !== "boolean" ? (shadowCopy[key] ? shadowCopy[key].toString() : "false") : shadowCopy[key]) + "" != window.localStorage[key]) {
                // Change detected!
				// Create StorageEvent
				StorageEvent(key, window.localStorage[key], shadowCopy[key] ? shadowCopy[key] : null);
            }
            if (window.localStorage.hasOwnProperty === undefined || window.localStorage.hasOwnProperty(key)) {
				newShadowCopy[key] = window.localStorage[key];            
            }
            delete shadowCopy[key];
        }
        for (var key in shadowCopy) {
            if (shadowCopy.hasOwnProperty(key)) {
                // Deletion of key detected!   
				StorageEvent(key, null, shadowCopy[key]);
            }
        }
        shadowCopy = newShadowCopy;
        //console.log("__eventListener run");
    }
		
	/**
	 *	Updates cached version of polyfilled localStorage object
	 *	and saves any changes add directly to the localStorage
	 *	object
	 *
	 *	@method __get
	 */	
	polyfill.prototype.__get = function () {
		storageHandle.load("cache");
		var rawData = storageHandle.getAttribute("polyfilled");
		polyfill.prototype.size = rawData ? rawData.length : 0;
		rawData = rawData ? rawData.split("\n") : [];
		polyfill.prototype.length = rawData.length;
		
		var polyfillVariables = {};
		for (var key in this) {
			if (this.hasOwnProperty(key)) {
				polyfillVariables[key] = true;
			}
		}
		
		for (var i = 0; i < rawData.length; i++) {
			var tmpRow = rawData[i].split(":");
			var key = unescape(tmpRow[0]);
			this[key] = unescape(tmpRow[1]);
			if (polyfillVariables[key] !== undefined)
				delete polyfillVariables[key];
		}
		
		for (var key in polyfillVariables) {
			delete this[key];
		}
	}
	
	/**
	 *	Creates a polyfilled StorageEvent and sends it to any
	 *	external listeners.
	 *
	 *	@method StorageEvent
	 *	@private
	 *	@param key {String} key of item to changed
	 *	@param newValue {String} new value of item changed
	 *	@return oldValue {String} old value of item changed
	 */
	function StorageEvent (key, newValue, oldValue) {
		//console.log("new storage event");
		var eventObj = document.createEventObject();
		eventObj.key = key;
		eventObj.newValue = newValue ? newValue : null;
		eventObj.oldValue = oldValue ? oldValue : null;
		eventObj.storageArea = window.localStorage;
		for (var i = 0; i < eventListeners.length; i++) {
			try {
				eventListeners[i](eventObj);
			} catch (e) {
				console.log(e.message, e);
			}
		}
	}
	
	polyfill.prototype.size = 0;

	polyfill.prototype.length = 0;
	
	// Should we polyfill localStorage?
	if (!window.localStorage) {

		// Create dom element to use as storage medium
		var storageHandle = document.createElement("input");
		document.getElementsByTagName("head")[0].appendChild(storageHandle);
		storageHandle.style.behavior = "url('#default#userData')";
		try {
			storageHandle.load("cache");
		} catch (e) {
			// userData not supported...
			console.log("UserData not supported");
			return false;
		}
		
		window.localStorage = new polyfill();
		window.localStorage.__get();
	}
	
	// Should we polyfill addEventListener?
    if (typeof window.addEventListener !== "function") {

	    var shadowCopy = {};
    	for (var key in window.localStorage)
    	    if (window.localStorage.hasOwnProperty === undefined || window.localStorage.hasOwnProperty(key))
		        shadowCopy[key] = window.localStorage[key];
	        
    	var eventListenerInterval = setInterval(__eventListener, 2000);
    
    	var eventListeners = [];
    	window.addEventListener = function (type, event) {
		    if (type == "storage" || type == "onstorage") {
			    eventListeners.push(event);
		    } else {
			    window.attachEvent(type, event);
		    }
    	}
    }
    	
})(window, document)

/* if (window.addEventListener) {
  window.addEventListener("storage", handle_storage, false);
} else {
	window.attachEvent("onstorage", handle_storage, false);
}

function handle_storage(evt){
    console.log("Storage event called", evt);
    console.log(evt.key + " changed from ", evt.oldValue + " to ", evt.newValue);
}
*/
