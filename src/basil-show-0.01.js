/**
 *
 *	#show helper
 *
 *	Will hide or show an element, if the statement passed to it is valid
 *
 */	
BasilJS.prototype.helpers["show"] = function (statement, e) {
	var valid = this.Utils.Template.getVariable(statement);
	
	console.log(this.instance, valid, statement, e);
	
	this.addListener(statement);
	
	if (valid === undefined || !valid || valid == "")
		e.style.display = 'none';
	else
		e.style.display = '';
		
	e.setAttribute("data-modifiedcount", this.instance);
};

// Sets the helper type to a "modifier"
// 
// This means you are handed an element, and you can manipulate it
// directly. DO NOT REMOVE IT. IT WILL NOT COME BACK!!
//
BasilJS.prototype.helpers["show"].prototype.access = "modify";