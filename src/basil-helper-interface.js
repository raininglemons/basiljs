/**
 *
 *	DOM manipulater
 *
 *	This is a helper that has the ability to add / remove dom elements.
 *	Should return either null or a DOMElement
 *
 *	Uses:
 *		- Removing / adding child nodes
 *		- Removing / adding element in question on page
 *		- Replacing element with another type.
 *		- etc.
 *
 *	DOMElement helper_function(DOMString statement, DOMElement e);
 *
 */

{
	element: ,
	iteration: ,
	Utils: ,
	addListener:
}

BasilJS.prototype.helpers["helper_name"] = helper_function;
BasilJS.prototype.helpers["helper_name"].prototype.access = "manipulate";


/**
 *
 *	DOM modifier
 *
 *	This is a helper that has the ability to modify a dom element.
 *	Should directly manipulate the passed "e" element.
 *
 *	Uses:
 *		- CSS changes
 *		- Adding / modifiying / removing attributes on element
 *
 *	void helper_function(DOMString statement, DOMElement e);
 *
 */


BasilJS.prototype.helpers["helper_name"] = helper_function;
BasilJS.prototype.helpers["helper_name"].prototype.access = "modify";